---
layout: markdown_page
title: Internal Support for GitLab Team Members
---

## Internal Support for GitLab Team Members
{:.no_toc}

**Looking for technical support? Please visit the [Support Page](/support/) instead.**
{:.no_toc}

----

### On this page
{:.no_toc}

- TOC
{:toc}

## What does the Support Team do?

GitLab Support provides technical support to our Self-managed and GitLab.com customers for the GitLab product.

We are *not* internal IT Support, so we probably can't help you with your MacBook, 1Password or similar issues. (Check in with `#peopleops` for who to contact regarding these problems.)

If you're a customer (or advocating for a customer), you should take a look at the dedicated [Support Page](/support) that describes how to get in
contact with us.

Fellow GitLab team-members can reach out for help from the Support Team in various ways:


1. For general support questions ("Can GitLab do x?", "How do I do y with GitLab?") try:
   - posing your question on the `#questions` channel in Slack, so that everyone can contribute to an answer. If you're not getting an answer, try cross-posting in the [relevant support team channel](#support-chat-channels).
1. For longer term or larger scope questions, such as team process change, or data gathering requests, create a [support issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues)
1. If customers or users have questions, advise them to contact support directly via the [support web form](https://support.gitlab.com).
1. As a last resort, ping the support team on one of the [support channels](#support-chat-channels).

### Support Issue Tracker

The [Support project](https://gitlab.com/gitlab-com/support/support-team-meta/issues) hosts an issue tracker meant to improve our workflow by reporting any problems that may arise in our tools or processes. It's also meant to propose and discuss ideas in general.

The issue tracker is open to the community and part of the `gitlab-com` group. It should not contain any sensitive information. Links to Zendesk or other references are encouraged.

### Support Chat Channels

The support channels are as follows:

- [#questions](https://gitlab.slack.com/messages/questions) - If your question is something that you think anyone in the company could answer or is valuable to the greater company to know, ask it here!
- [#zd-self-managed-feed](https://gitlab.slack.com/messages/C1CKSUTL5/) - Feed of all self-managed Zendesk ticket activities.
- [#zd-gitlab-com-feed](https://gitlab.slack.com/messages/CADGU8CG1/) - Feed of all GitLab.com Zendesk ticket activities.
- [#support-managers](https://gitlab.slack.com/messages/CBVAE1L48/) - This channel is specifically for support managers.
- [#support_self-managed](https://gitlab.slack.com/messages/support_self-managed/) - This channel is specifically for the self-managed support team. They handle self-managed production issues, triage bugs, and self-managed emergencies, among other things.
- [#support_gitlab-com](https://gitlab.slack.com/messages/C4XFU81LG/) - This channel is specifically for the GitLab.com support team. They handle GitLab.com account and subscription support and GitHost.
- [#githost](https://gitlab.slack.com/messages/githost/) - This channel handles monitoring for GitHost instances.

In order to attract support team's attention in Slack, you can use the team handles, mentioning multiple team members in a message or a thread where support is needed. Support team handles are:
- `@support-selfmanaged` - Self-managed support team members.
- `@support-dotcom` - GitLab.com support team members.
- `@supportmanagers` - Support director and managers.

## Internal Requests

### Note on Zendesk and support@gitlab.com 

All internal requests to support should follow the guidelines set in the rest of this section. Please do not open requests directly through [Zendesk](https://support.gitlab.com) or by sending an email to support@gitlab.com. If customers have a support question, please direct them to open a ticket either through the [Support Portal](https://support.gitlab.com) or via email.

### Note on Requesting Customer Information

According to our [privacy policies](https://about.gitlab.com/privacy/), the support team will not provide any information regarding customers, groups, projects, etc, that are not available publicly. This includes situations where a customer is requesting information about their own projects, groups, etc. If they are unable to authenticate, we cannot assume they are who they say they are. If they are locked out, please have them submit a ticket to the support team for assistance.

### 'Light Agent' Zendesk accounts available for all GitLab staff

All GitLab staff can request a 'Light Agent' account so that you can see customer tickets in Zendesk and leave notes for the Support team. These accounts are free. 

To request a Light Agent Zendesk account, please send an email to <mailto:gitlablightagent.5zjj2@zapiermail.com> - you'll receive an automated reply with the result of your request. **NOTE:** you must send your request from your GitLab Google / Gmail account. No other addresses will work. The Subject and Body fields of the email can be empty.

Once set up, you can sign into Zendesk using your GitLab Google account credentials at: <https://gitlab.zendesk.com/agent>

You cannot send public replies to customers with a Light Agent account - if you need to do this, please submit a [new Access Request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) for a paid full agent account. 

[Read more information on Light Agent accounts from Zendesk](https://www.zendesk.com/company/collaboration-add-on-additional-features/)

### Common internal requests - all GitLab team-members

##### I want Gold for my work or personal account
- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=gitlabber_gold_request&issue[title]=GitLab.com%20Gold%20tier%20request:%20@username) using the `GitLab team-member Gold Request` template.

##### I want to claim a dormant username
- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=dormant_name_request) using the `Dormant Name Request` template.

##### I need access to something
- Support doesn't handle these requests so you'll want to open a new [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) issue.

##### I need Support to contact a user on GitLab's behalf
- If required, you can [request us to contact a user](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=request_contact). Typically, this is for the production team to notify actions taken on behalf of a user.

### Common internal requests - Sales Team / Technical Account Managers

##### I want to extend or change the plan of a GitLab.com Trial
- For trial extensions [open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=trial_extension) using the `Trial Extension` template.

- To grant a Bronze or Silver trial [open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request) using the `Plan Change Request` template. 

>**NOTE**: It's not yet possible for a user or GitLab employee to directly start a Bronze or Silver trial. If one is needed, have the user initiate a Gold trial and then open an issue per the above link using the `Plan Change Request` template to have the plan manually changed. We're tracking this in [customers-gitlab-com#409](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/409)```

##### My prospect is unable to apply a purchased subscription to their GitLab.com group
- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request) using the `Plan Change Request` template.

>**NOTE**: The issue is most likely that their group still has an active trial which must be expired manually first. Open an issue using the above template to have that done. This should only be necessary until an active trial is expired automatically when a subscription is applied to a namespace. Keep an eye on [this issue](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/482) to track progress on the fix.

##### I want to change the plan of a GitLab.com user or group
- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request) using the `Plan Change Request` template.

##### I want to see if a group name is free or able to be claimed for a potential customer
- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=dormant_name_request) using the `Dormant Name Request` template.

>**NOTE**: Using another's trademark in a way that has nothing to do with the product or service for which the trademark was granted is not a violation of trademark policy. User and group names are provided on a first-come, first-served basis and may not be reserved. We can ask nicely to free an active namespace, but we won't take anything away. Your prospects desired group name may not be freeable.

##### I want to draw attention to an existing support ticket
1. Review the SLA associated with the account and the amount of time left until a breach by logging into https://gitlab.zendesk.com with your GSuite credentials. (It's not typically necessary to escalate an issue that is hours away from a breach)
1. Post a link to the ticket and a reason for why this ticket needs special attention into `#support-managers`. Feel free to tag `@supportmanagers`.
1. Understand that we'll do our best to prioritize appropriately taking into account all of the tickets in the queues - there may be more pressing items.

##### I want to know who is on-call today
- You can run `/chatops run oncall support` in `#support_self-managed` to see who is on-call. This will **not** page the on-call person.

##### I want to schedule a customer call for upgrade assistance 
- [Open an issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/new?issuable_template=Customer%20call) using the `Customer Call` template.

##### I want to request that pipeline minutes be reset for a user or group

- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=pipeline_reset_request) using the `Pipeline Reset Request` template.

### Common internal requests - Legal
##### I need logs preserved pursuant to a subpoena or other formal legal request
- [Open an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=information_request) using the `Information Request` template.

##### I need to submit a DMCA request
- [Open an issue](https://gitlab.com/gitlab-com/gl-security/abuse/issues/new?issuable_template=dmca_meta_issue) using the `DMCA Meta Issue` template

