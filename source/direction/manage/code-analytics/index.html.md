---
layout: markdown_page
title: "Category Strategy - Code Analytics"
---

- TOC
{:toc}

## Manage Stage

| Stage| Maturity |
| --- | --- |
| [Manage](/direction/manage/) | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thank you for visiting the category strategy page for Code Analytics. This page belongs to [Virjinia Alexieva](https://gitlab.com/valexieva) ([E-Mail](mailto:valexieva@gitlab.com), [Twitter](https://twitter.com/virjinialexieva)).

This strategy is a work in progress and everyone can contribute by sharing their feedback directly on [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1479), via e-mail, or Twitter.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
While we strive to provide recommendations and checks around [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) in real time and through static analysis, we believe there are a lot more insights about our code that can be drawn from our repositories. Moreover, our codebase is usually a complex system, which has to be understood well in order to be managed well under time and money constraints. With more and more companies adopting agile practices and suffering from high attrition, understanding and modifying code can become a challenging task.


### Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->
We would like to help Engineering Managers identify problematic patterns and prioritize changes to the codebase that would provide the most value. In order to do that, we will start with addressing the following questions:

- Are there parts of the codebase where most of our defects are coming from?
- Are these volatile areas of the code with considerable churn where engineers spend a lot of effort? What is the reason for these hotspots?
- Do hotspots attract a lot of engineers and do they work on them in parallel?
- Who are our main developers for each file? How can we ensure we tackle knowledge loss?
- Where is our abandoned code and how do we define it? Was it written by engineers that have already left the company?
- What is our test coverage across repositories?
- How do we make prioritization of technical debt more data driven?
- How does code complexity trend over time and is it concentrated in particular segments?
- How does our use of different languages and versions trend over time and how does it correlate with quality?

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
We believe the target audience for Code Analytics is Engineering Management, who seeks to understand their codebase and to take data driven prioritization and management decisions. 

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
In our first release containing [Code Analytics MVC](https://gitlab.com/gitlab-org/gitlab-ce/issues/62372), we will provide a definition and visual representation of hotspots as well as language usage trends. 

<!-- 
### What is Not Planned Right Now
Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

### Maturity Plan
This category is currently at the `Minimal` maturity level and our next maturity target is `Viable` by October 2019. Please see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend) and related [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=group%3A%3Aanalytics&label_name[]=devops%3A%3Amanage&label_name[]=Category%3A%3ACode%20Analytics).

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

Epic for a [Minimal Maturity](https://gitlab.com/groups/gitlab-org/-/epics/1479)
Epic for a [Viable Maturity](https://gitlab.com/groups/gitlab-org/-/epics/1480)


This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

#### [Code Scene](https://empear.com)
#### [Sonar Source](https://www.sonarsource.com)
#### [Codacy](https://www.codacy.com)
#### [Code Climate](https://codeclimate.com)

<!-- 
### Analyst Landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- 
### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->
[Visualize the number of lines of code](https://gitlab.com/gitlab-org/gitlab-ce/issues/31010) and trends thereof for each language in our repositories.

<!-- 
### Top internal customer issue(s)
These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->


<!--
### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
