---
layout: job_family_page
title: “ Revenue Manager”
---

The Revenue Manager is responsible for all day-to-day revenue accounting operations and month-end financial reporting requirements while ensuring compliance with revenue recognition policies, procedures and internal controls. The Revenue Manager will report to the Controller and will help drive process improvement projects as Gitlab continues to grow. 


## Responsibilities
- Review and summarize customer contracts for proper revenue recognition in accordance with company policy and revenue recognition accounting standards. 
- Partner with Legal department and business teams to interpret complex or unique contract provisions or new revenue initiatives. Research and document accounting issues and recommend solutions for complex transactions as needed.
- Key contributor in the monthly financial close, ensuring accurate and timely recording of transactions and the completeness of financial statements.
- Work closely and help manage transactions with the billing and deal desk teams
- Prepare monthly account reconciliations, entries and contribute to the financial reporting process
- Develop and document processes and controls, and identify opportunities for streamlining processes and increasing automation to improve the speed and efficiency of the monthly revenue close
- Support external and internal audit requirements 
- Work as liaison with project teams to develop or improve finance solutions, including the design, implementation and review of systems
- Assist with and manage various ad hoc projects as needed

## Requirements
- 2+ years experience in revenue accounting with a Big 4 accounting firm or SaaS company
- Experience in applying revenue accounting guidance (ASC 606).  Strong knowledge of US GAAP, Sarbanes-Oxley, risk and controls standards and business process best practices skills
- Experience managing critical projects such as system implementations, ERP system enhancements, and system upgrades.
- Strong understanding of process and operations in a fast-paced business environment.
- Ability to work dynamically with a variety of internal business partners (financial and non-financial personnel both within the US and abroad).
- Be a self-starter, have a positive attitude, maintain a solution-oriented work ethic.
- Zuora, RevPro, Netsuite system knowledge


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).
- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to meet with two other members of the billing team
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
