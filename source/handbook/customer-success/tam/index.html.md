---
layout: markdown_page
title: "Technical Account Management"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

- Technical Account Manager Summary *(Current)*
- [Account Triage](/handbook/customer-success/tam/triage)
- [Account Engagement](/handbook/customer-success/tam/engagement)
- [Account Onboarding](/handbook/customer-success/tam/onboarding)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Gemstones](/handbook/customer-success/tam/gemstones)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Customer Health Scores](https://about.gitlab.com/handbook/customer-success/tam/health-scores/)
- [Customer Renewal Tracking](https://about.gitlab.com/handbook/customer-success/tam/renewals/)

---

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, Professional Services Engineers and support.

See the [Technical Account Manager role description](/job-families/sales/technical-account-manager/) for further information.

### Advocacy
A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product.  In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base. Quarterly, the TAM team meets to deliver a [Top 10 list](/handbook/customer-success/tam/cs-top-10) to the [Product](/handbook/product) team.

### Values

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption, training and regular healthchecks.

### Responsibilities and Services
All Premium customers with a minimum Annual Recurring Revenue (ARR) of $100,000 are aligned with a Technical Account Manager. There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

#### Relationship Management
* Regular cadence calls
* Regular open issue reviews and issue escalations
* Account healthchecks
* Quarterly business reviews
* Success strategy roadmaps - beginning with a 30/60/90 day success plan
* To act as a key point of contact for guidance, advice and as a liaison between the customer and other GitLab teams
* Own, manage, and deliver the customer onboarding experience
* Help GitLab's customers realize the value of their investment in GitLab
* GitLab Days

#### Training
* Identification of pain points and training required
* Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
* "Brown Bag" trainings
* Regular communication and updates on GitLab features
* Product and feature guidance - new feature presentations

#### Support
* Upgrade planning
* User adoption strategy
* Migration strategy and planning
* Launch support
* Monitors support tickets and ensures that the customer receives the appropriate support levels
* Support ticket escalations

It is also possible for a customer to pay for a Technical Account Manager's services in order to receive priority, "white glove" assistance, guidance and support as well as more time allocated to their account on a monthly basis. There are also additional services a Technical Account Manager will provide to the services listed above.
